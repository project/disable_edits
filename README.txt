Disable Edits Module
====================

Description
-----------

The Disable Edits module is a simple administration module that allows you to 
configure paths that should be redirected to a page of your choosing when 
the checkbox enabling this behavior is checked. The idea is preventing users
from adding new content. The most obvious example use case for this module is
during a migration, it may be desirable to disallow adding/editing content or
users, but otherwise keeping the site out of maintenance mode for viewing. 


Installation
------------

See https://www.drupal.org/docs/7/extending-drupal/installing-modules

Author
------

Written by: Daniel Harris of Webdrips (http://webdrips.com)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.
